# -*- coding: utf-8
import logging
import os

import telebot
from telebot import types
from telebot.types import Message

log = logging.getLogger('bot')
log.addHandler(logging.StreamHandler())
log.setLevel(logging.DEBUG)
import time

from pynamodb.models import Model
from pynamodb.attributes import NumberAttribute, JSONAttribute

from raven import Client
from raven.transport.http import HTTPTransport
sentry = Client(os.getenv('SENTRY_URL'), transport=HTTPTransport)

def sentry_exceptions(handler):
    def wrapped(*args, **kwargs):
        if os.getenv('SENTRY_URL'):
            try:
                return handler(*args, **kwargs)
            except Exception:
                sentry.captureException()
        else:
            return handler(*args, **kwargs)

    return wrapped

class UserContext(Model):
    """
    A DynamoDB User
    """
    class Meta:
        table_name = "2an-usercontext"

    chat_id = NumberAttribute(hash_key=True)
    body = JSONAttribute(default=dict(state=None))

    @classmethod
    def get_or_create(cls, key):
        try:
            return cls.get(key)
        except cls.DoesNotExist:
            ret = cls(key)
            ret.save()
            return ret

UserContext.create_table(read_capacity_units=1, write_capacity_units=1)

if os.getenv('MOCK_REGISTER', False):
    def register(*args, **kwargs):
        log.debug('mock register: {0}'.format(args))
        time.sleep(0.3)
        return True
else:
    from an2 import register


TOKEN = os.getenv('TELEGRAM_TOKEN')
bot = telebot.TeleBot(TOKEN)

admins = [
    226050421,  # @pavel64
    192698956,  # @sahno
    #326442468,  # Дмитрий Захаров
    129170489,  # Куракин
    213548933,  # Никита Гуц
    343746977,  # Danila
    94589103,   # Марина Матвеева
    205487819,  # Алексей маркетолог
    321873834,  # Людмила Соболева
]

def message_logger(handler):
    def wrapped(*args, **kwargs):
        if args and isinstance(args[0], Message):
            message = args[0]
            log.info('< [id={0} username={1}] {2}'.format(
                message.chat.id,
                message.chat.username,
                repr(message.text)))

        if 'chat_id' in kwargs:
            chat_id, text = kwargs['chat_id'], kwargs['text']
            log.info('> [id={0}] {1}'.format(chat_id, repr(text)))

        return handler(*args, **kwargs)

    return wrapped


def reply_keyboard_remover(handler):
    def wrapped(*args, **kwargs):
        if not 'reply_markup' in kwargs:
            kwargs['reply_markup'] = types.ReplyKeyboardRemove()

        return handler(*args, **kwargs)

    return wrapped


bot.send_message = \
    reply_keyboard_remover(
        message_logger(
            bot.send_message
        )
    )

@sentry_exceptions
@bot.message_handler(commands=['start'])
@message_logger
def start(message):
    if message.chat.id in admins:
        bot.send_message(chat_id=message.chat.id,
                         text='Привет, администратор. \n\nЧтобы выписать пропуск, используй команду /pr')
    else:
        try:
            _, token = message.text.split()
            log.info('start token = {0}'.format(token))
        except ValueError:
            bot.send_message(chat_id=message.chat.id,
                             text='Привет, это чат-бот АнтиНормы.\n\nДля начала работы оставь свои контактные данные на сайте http://antinorma.ru/')
            return


import datetime

plushkas = set()
already = set()
import locale
locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')

def workingdays(start=datetime.date.today()):
    while start.weekday() < 5:
        yield start
        start += datetime.timedelta(1)
        # print(start)


def alldays(start=datetime.datetime.today().date()):

    dates = [start + datetime.timedelta(1)
             for n in range(7)]
    return dates

def next_weekday(d, weekday):

    days_ahead = weekday - d.weekday()
    if days_ahead <= 0: # Target day already happened this week
        days_ahead += 7
    return (d + datetime.timedelta(days_ahead))

def waiting_date(bot, message):
    dates = [datetime.datetime.today().date() + datetime.timedelta(days=n)
             for n in range(9)]

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
    markup.row(types.KeyboardButton(
        'сегодня {0}'.format(dates[0].strftime('%d.%m'))))
    dates_captions = []
    this_week = list(workingdays())
    this_week2 = list(alldays())
    # print(list(this_week))
    next_week = list(
        workingdays(next_weekday(datetime.datetime.today().date(), 0)))

    next_week2 = list(
        alldays(next_weekday(datetime.datetime.today().date(), 0)))
    # print(len(this_week))
    # return

    if this_week:
        dates_captions += ['пн-пт {0}-{1}'.format(this_week[0].strftime('%d.%m'),
                                                  this_week[-1].strftime('%d.%m'))]
    dates_captions += ['пн-пт {0}-{1}'.format(next_week[0].strftime('%d.%m'),
                                              next_week[-1].strftime(
                                                  '%d.%m'))]

    if this_week2:
            dates_captions += ['пн-вс {0}-{1}'.format(this_week2[0].strftime('%d.%m'),
                                                  this_week2[-1].strftime('%d.%m'))]
    dates_captions += ['пн-вс {0}-{1}'.format(next_week2[0].strftime('%d.%m'),
                                              next_week2[-1].strftime(
                                                  '%d.%m'))]
    markup.row(*(types.KeyboardButton(cap) for cap in dates_captions))

    dates_captions = []
    dates_captions += [d.strftime('%a %d.%m'.lower()) for d in dates[1:]]

    markup.add(*(types.KeyboardButton(cap) for cap in dates_captions))



    bot.send_message(chat_id=message.chat.id,
                     text='Выберите дату посещения',
                     reply_markup=markup)


def waiting_fio_confirm(bot, message, fios, date):
    table = []
    if isinstance(date, list):
        table += ['Даты посещений: {0}-{1}'.format(date[0], date[-1])]
    else:
        table += ['Дата посещения: {0}'.format(date)]

    table += ['']
    table += ['{0:15}| {1:10}| {2:10}'.format('Фамилия', 'Имя', 'Отчество')]
    table += ['{0:15}+-{1:10}+-{2:10}'.format('-' * 15, '-' * 10, '-' * 10)]
    for fio in fios:
        print(fio)
        table += ['{0:15}| {1:10}| {2:10}'.format(*fio)]

    table += ['']
    table += ['Итого: {0} шт'.format(len(fios))]

    print(table)

    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.row(types.KeyboardButton('Да'),
               types.KeyboardButton('Нет'))
    markup.row(types.KeyboardButton('Имя ↔ Фамилия'))

    if len(fios) >= 2:
        table = '\n'.join(table)
    else:
        if isinstance(date, list):
            table = '\n'.join([
                'Даты посещений: {0}-{1}'.format(date[0], date[-1]),
                ' '.join(fios[0])])
        else:
            table = '\n'.join(['Дата посещения: {0}'.format(date),
                               ' '.join(fios[0])])

    bot.send_message(chat_id=message.chat.id, text=
    '```\n{table}```\n\nВсё верно?'.format(
        **locals()),
                     parse_mode='Markdown',
                     reply_markup=markup)


@sentry_exceptions
@message_logger
def pr(message):
    """Выписать пропуск"""
    if message.chat.id in admins:
        uc = UserContext.get_or_create(message.chat.id)
        waiting_date(bot, message)
        uc.body['state'] = 'waiting_date'
        uc.save()

import re
@sentry_exceptions
@message_logger
def message(message):
    if message.chat.id in admins:
        uc = UserContext.get_or_create(message.chat.id)
        if uc.body['state'] == 'waiting_date':

            if os.getenv('MOCK_DATE'):
                date = datetime.datetime(2020,2,2).strftime('%d.%m.%Y')
            else:
                try:
                    day1, month1, day2, month2 = re.compile(
                        '.*(\d{2})\.(\d{2})-(\d{2})\.(\d{2})').match(
                        message.text).groups()
                    print(message.text[:5])
                    if message.text[:5] != "пн-вс":
                        date = [d.strftime('%d.%m.%Y') for d in workingdays(
                            datetime.date(year=datetime.datetime.today().year,
                                          month=int(month1), day=int(day1)))]
                    else:
                        date = [d.strftime('%d.%m.%Y') for d in alldays(
                            datetime.date(year=datetime.datetime.today().year,
                                          month=int(month1), day=int(day1)))]
                except (ValueError, AttributeError):
                    try:
                        day, month = re.compile('.*(\d{2})\.(\d{2})').match(message.text).groups()
                        date = datetime.date(year=datetime.datetime.today().year,
                                                 month=int(month), day=int(day)).strftime('%d.%m.%Y')
                    except (ValueError, AttributeError):
                        waiting_date(bot, message)
                        uc.body['state'] = 'waiting_date'
                        uc.save()
                        return

            bot.send_message(
                chat_id=message.chat.id,
                text='Пожалуйста, напишите один или несколько ФИ[О] для пропуска, например:\n\n```\n'
                     'Иванов Иван Иванович\nПетров Николай\nСиницын Григорий нет\n```',
                parse_mode='Markdown')

            uc.body['date'] = date

            uc.body['state'] = 'waiting_fio'
            uc.save()

        elif uc.body['state'] == 'waiting_fio':
            fios = []
            for line in message.text.splitlines():
                try:
                    f, i, o = line.strip().split()[:3]
                except ValueError:
                    try:
                        f, i = line.strip().split()[:2]
                        o = 'нет'
                    except ValueError:
                        bot.send_message(chat_id=message.chat.id,
                                         text='Некоторые строки введены с ошибками, попробуйте еще раз')
                        return
                fios.append([f,i,o])

            print(fios)
            print('\n'.join([' '.join(fio) for fio in fios]))

            waiting_fio_confirm(bot, message,
                                fios=fios, date=uc.body['date'])

            uc.body['fios'] = fios
            uc.body['state'] = 'waiting_fio_confirm'
            uc.save()

        elif uc.body['state'] == 'waiting_fio_confirm':
            if message.text == 'Да':

                fios = uc.body['fios']
                date = uc.body['date']
                index = 1
                for f, i, o in fios:
                    bot.send_message(chat_id=message.chat.id,
                                     text='Выписываю пропуска ({0} из {1})...'.format(index, len(fios)))
                    bot.send_chat_action(chat_id=message.chat.id,
                                         action='typing')
                    if not register(f, i, o, date):
                        bot.send_message(chat_id=message.chat.id,
                                         text='Не удалось выписать пропуск, попробуйте позднее')
                        log.error('Error registering')
                        return
                    index += 1

                bot.send_message(chat_id=message.chat.id,
                                 text='Ок, выписаны пропуска по списку (для {0} человек на {1})'.format(
                                     len(fios),
                                     '{0} дней'.format(len(date)) if isinstance(date, list) else date))

                uc.body['state'] = None
                uc.save()
            elif message.text == 'Имя ↔ Фамилия':
                uc.body['fios'] = [[fio[1], fio[0], fio[2]] for fio in uc.body['fios']]
                waiting_fio_confirm(bot, message,
                                    fios=uc.body['fios'], date=uc.body['date'])

                uc.body['state'] = 'waiting_fio_confirm'
                uc.save()
            else:
                waiting_date(bot, message)
                uc.body['state'] = 'waiting_date'


                uc.save()
                return
        else:
            if message.chat.id in admins:
                bot.send_message(chat_id=message.chat.id,
                                 text='Привет, администратор. \n\nЧтобы выписать пропуск на сегодня, используй команду /pr')
            else:
                try:
                    _, token = message.text.split()
                    log.info('start token = {0}'.format(token))
                except ValueError:
                    bot.send_message(chat_id=message.chat.id,
                                     text='Привет, это чат-бот АнтиНормы.\n\nДля начала работы оставь свои контактные данные на сайте http://antinorma.ru/')
                    return



from flask import Flask, request
server = Flask(__name__)

@server.route("/bot/", methods=['POST'])
def getMessage():
    update_body = request.stream.read().decode("utf-8")
    print(update_body)
    update = telebot.types.Update.de_json(update_body)
    if update.message.text == '/start':
        start(update.message)
    elif update.message.text == '/pr':
        pr(update.message)
    else:
        message(update.message)
    # bot.send_message(update.message.chat.id, text='got ya')
    return "!", 200


#@server.route("/")
#def webhook():
    #bot.remove_webhook()
    #bot.set_webhook(url=os.getenv('WEBHOOK_URL'))
    #return "!", 200

# for k, v in os.environ.items():
#     print (k, v)

if __name__ == '__main__':
    bot.message_handler(commands=['start'])(start)
    bot.message_handler(commands=['pr'])(pr)
    bot.message_handler()(message)
    bot.polling()
